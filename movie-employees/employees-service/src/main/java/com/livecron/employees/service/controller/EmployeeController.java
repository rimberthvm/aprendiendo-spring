package com.livecron.employees.service.controller;

import com.livecron.employees.api.input.EmployeeCreateInput;
import com.livecron.employees.service.model.domain.Employee;
import com.livecron.employees.service.model.service.EmployeeCreateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Rimberth Villca
 * rimbervm@gmail.com
 */
@RequestMapping(value = "/employees")
@RequestScope
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeCreateService employeeCreateService;

    @ApiOperation(
            value = "This endpoint is to create account"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "Employee created"),
            @ApiResponse(code = 403, message = "Acceso denegado")
    })
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput input) {
        employeeCreateService.setInput(input);
        employeeCreateService.execute();

        return employeeCreateService.getEmployee();
    }
}
