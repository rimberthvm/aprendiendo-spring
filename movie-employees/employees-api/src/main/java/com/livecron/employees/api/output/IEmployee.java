package com.livecron.employees.api.output;

import java.util.Date;

/**
 * @author Rimberth Villca
 * rimbervm@gmail.com
 */
public interface IEmployee {

    Long getId();

    String getFirstName();

    String getLastName();

    Boolean getActive();

    Date getCreatedDate();

    Long getUserId();

}
